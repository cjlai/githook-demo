package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AppExampleServiceTest {

	@Autowired
	private AppExampleService appExampleService;

	@Test
	void test() {

		String expected = "FRANK";

		String actual = appExampleService.howAmI("Frank");

		Assertions.assertEquals(expected, actual);
	}

}
