package com.example.demo;

import org.springframework.stereotype.Service;

@Service
public class AppExampleService {

	public String howAmI(String name) {

		if (null != name && !name.isEmpty()) {

			return name.toUpperCase();
		}
		return "Not Found";
	}
}
